"""
A string can be compressed only if adjacent characters are equal.
I try to greedily group adjacent characters until they are not equal, then compress them.
The end of the string is denoted by '$'.

The Time Complexity of compress is big oh (length of string)

"""


def compress(s):
    p1 = 0
    p2 = 1
    s += '$'
    while(p2 < len(s)):
        if(s[p2] != s[p2 - 1]):
            if p2 - p1 == 1:
                print(s[p1], end='', sep='')
            else:
                print(s[p1], p2 - p1, end='', sep='')
            p1 = p2
        p2 += 1


s = input()
compress(s)