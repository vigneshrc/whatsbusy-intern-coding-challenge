"""
The identify router performs dfs (depth first search) to find indegree and outdegree of all nodes
The maximum indegree  + outdegree is the failure point
The failure points are stored in a list and printed

Time Complexity is big oh (v + e)
v is the number of vertices
e is the number of edges
As number of vertices is very small 
it is O (e)

"""
from collections import defaultdict

# This class represents a directed graph using
# adjacency list representation


class Graph:

    # Constructor
    def __init__(self):

        # default dictionary to store graph
        self.graph = defaultdict(list)

    # function to add an edge to graph
    def addEdge(self, u, v):
        self.graph[u].append(v)

    # A function used by DFS
    def DFSUtil(self, v, visited, indegree, outdegree):

        # Mark the current node as visited
        visited[v] = True

        # Recur for all the vertices
        # adjacent to this vertex
        # increment indegree, outdegree to find failure point
        for i in self.graph[v]:
            if not visited[i]:
                self.DFSUtil(i, visited, indegree, outdegree)
            indegree[i] += 1
            outdegree[v] += 1


# Identify router that causes maximum damage
def identify_router(g, n):
    visited = [False] * (n + 1)
    indegree = [0] * (n + 1)
    outdegree = [0] * (n + 1)
    for i in range(1, n + 1):
        if visited[i] == 0:
            g.DFSUtil(i, visited, indegree, outdegree)
    max_connections = 0
    for i in range(1, n + 1):
        max_connections = max(max_connections, indegree[i] + outdegree[i])
    nodes = []
    for i in range(1, n + 1):
        if indegree[i] + outdegree[i] == max_connections:
            nodes.append(i)
    return nodes


# Driver code
# Create a graph given

g = Graph()
n = 6

network = [int(x) for x in input().split(' -> ')]
for i in range(len(network) - 1):
    g.addEdge(network[i], network[i + 1])

ans = identify_router(g, n)
print(ans)